#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os

import numpy as np

import tensorflow as tf
assert tf.__version__.startswith('2')

from tflite_model_maker import model_spec
from tflite_model_maker import image_classifier
from tflite_model_maker.config import ExportFormat
from tflite_model_maker.config import QuantizationConfig
from tflite_model_maker.image_classifier import DataLoader

# 파이썬 시각화 패키지 불러오기
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# 사용자 운영체제 확인
import os
os.name


# In[2]:


# 운영체제별 한글 폰트 설정
if os.name == 'posix': # Mac 환경 폰트 설정
    plt.rc('font', family='AppleGothic')
elif os.name == 'nt': # Windows 환경 폰트 설정
    plt.rc('font', family='Malgun Gothic')

plt.rc('axes', unicode_minus=False) # 마이너스 폰트 설정

get_ipython().run_line_magic('config', "InlineBackend.figure_format = 'retina'")


# In[3]:


plt.plot([1,2,3,4])
plt.ylabel('한글 깨짐 확인')
plt.show()


# In[4]:


def showSample(data):
    plt.figure(figsize=(10,10))
    for i, (image, label) in enumerate(data.gen_dataset().unbatch().take(25)):
      plt.subplot(5,5,i+1)
      plt.xticks([])
      plt.yticks([])
      plt.grid(False)
      plt.imshow(image.numpy(), cmap=plt.cm.gray)
      plt.xlabel(data.index_to_label[label.numpy()])
    plt.show()


# In[5]:


def get_label_color(val1, val2):
  if val1 == val2:
    return 'black'
  else:
    return 'red'


# In[6]:


def showPredicted(data):
    plt.figure(figsize=(20, 20))
    predicts = model.predict_top_k(test_data)
    for i, (image, label) in enumerate(test_data.gen_dataset().unbatch().take(100)):
      ax = plt.subplot(10, 10, i+1)
      plt.xticks([])
      plt.yticks([])
      plt.grid(False)
      plt.imshow(image.numpy(), cmap=plt.cm.gray)
    
      predict_label = predicts[i][0][0]
      color = get_label_color(predict_label,
                          test_data.index_to_label[label.numpy()])
      ax.xaxis.label.set_color(color)
      plt.xlabel('Predicted: %s' % predict_label)
    plt.show()


# In[7]:


image_path = os.path.join('C:\\Users\\tmdgu\\.keras\\datasets', 'flower_photos')


# In[8]:


print("image_path:", image_path)


# In[9]:


data = DataLoader.from_folder(image_path)
train_data, test_data = data.split(0.9)


# In[10]:


showSample(data)


# In[11]:


model = image_classifier.create(train_data)


# In[12]:


loss, accuracy = model.evaluate(test_data)
showPredicted(test_data)


# In[13]:


model.export(export_dir = ".", with_metadata=False)


# In[ ]:




